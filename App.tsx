import React, {useEffect, useState} from 'react';
import {Client, Message} from 'react-native-paho-mqtt';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
} from 'react-native';
import {
  accelerometer,
  gyroscope,
  setUpdateIntervalForType,
  SensorTypes,
} from 'react-native-sensors';

setUpdateIntervalForType(SensorTypes.accelerometer, 1000);
setUpdateIntervalForType(SensorTypes.gyroscope, 1000);

const myStorage = {
  setItem: (key, item) => {
    myStorage[key] = item;
  },
  getItem: key => myStorage[key],
  removeItem: key => {
    delete myStorage[key];
  },
};

let client;

const App = () => {
  const [host, setHost] = useState(''); //wss://tailor.cloudmqtt.com:30499/
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [acc, setAcelerometer] = useState({x: 0, y: 0, z: 0, t: 0});

  const [connected, setConnected] = useState('Disconnected');

  const [accInterval, setAccInterval] = useState(1000);
  const [gyroInterval, setGyroInterval] = useState(1000);

  useEffect(() => {
    const subscription = accelerometer.subscribe(
      data => {
        const result = {
          x: data.x,
          y: data.y,
          z: data.z,
          t: data.timestamp,
        };
        setAcelerometer(result);
        if (client && client.isConnected()) {
          const message = new Message(JSON.stringify(result));
          message.destinationName = 'acc';
          client.send(message);
        }
      },
      error => null,
    );

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  const [gyro, setGyro] = useState({x: 0, y: 0, z: 0, t: 0});

  useEffect(() => {
    const subscription = gyroscope.subscribe(
      data => {
        const result = {
          x: data.x,
          y: data.y,
          z: data.z,
          t: data.timestamp,
        };
        setGyro(result);
        if (client && client.isConnected()) {
          const message = new Message(JSON.stringify(result));
          message.destinationName = 'gyro';
          client.send(message);
        }
      },
      error => null,
    );

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  const disconnect = () => {
    if (client && client.isConnected()) {
      client.disconnect();
    }
  };

  const connect = () => {
    disconnect();

    setConnected('Disconnected');
    client = new Client({
      uri: host, //'wss://tailor.cloudmqtt.com:30499/',
      clientId: 'clientID',
      storage: myStorage,
    });
    client
      .connect({userName: username, password: password})
      .then(() => {
        setConnected('Connected');
        //console.log('onConnect');
        //return client.subscribe('World');
      })
      .then(() => {})
      .catch(responseObject => {
        // if (responseObject.errorCode !== 0) {
        //   console.log('onConnectionLost:' + responseObject.errorMessage);
        // }
        setConnected('Failed');
      });

    client.on('connectionLost', responseObject => {
      if (responseObject.errorCode !== 0) {
        //console.log(responseObject.errorMessage);
        setConnected('Lost Connection');
      }
    });
    client.on('messageReceived', message => {
      //console.log(message.payloadString);
    });
  };

  const saveInterval = () => {
    setUpdateIntervalForType(SensorTypes.accelerometer, accInterval);
    setUpdateIntervalForType(SensorTypes.gyroscope, gyroInterval);
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View>
            <Text>Host</Text>
            <TextInput
              placeholder={'Host'}
              onChangeText={text => setHost(text)}
              value={host}
            />
            <Text>Username</Text>
            <TextInput
              placeholder={'Username'}
              onChangeText={text => setUsername(text)}
              value={username}
            />
            <Text>Password</Text>
            <TextInput
              placeholder={'Password'}
              onChangeText={text => setPassword(text)}
              value={password}
            />
            <Button title="Connect" onPress={connect} />
            <Text>{connected}</Text>
            <Button title="Disconnect" onPress={disconnect} />
          </View>
          <View>
            <Text>Accelerometer Interval</Text>
            <TextInput
              placeholder={'Milliseconds'}
              onChangeText={text => setAccInterval(parseInt(text))}
              value={accInterval.toString()}
            />
            <Text>Gyroscope Interval</Text>
            <TextInput
              placeholder={'Milliseconds'}
              onChangeText={text => setGyroInterval(parseInt(text))}
              value={gyroInterval.toString()}
            />
            <Button title="Save" onPress={saveInterval} />
          </View>
          <View>
            <View>
              <Text>accelerometer</Text>
              <Text>x: {acc.x}</Text>
              <Text>y: {acc.y}</Text>
              <Text>z: {acc.z}</Text>
              <Text>t: {acc.t}</Text>
            </View>
            <View>
              <Text>gyroscope</Text>
              <Text>x: {gyro.x}</Text>
              <Text>y: {gyro.y}</Text>
              <Text>z: {gyro.z}</Text>
              <Text>t: {gyro.t}</Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default App;
